%define ociver 19c
%define oraclever 19.3
%define oracleicname instantclient%{oraclever}
%define oracleicver 19.3.0.0.0
%define pkgname cx_Oracle

# On el8:
# the primary package built is 'cx_Oracle'
# the sub package built is 'python3-cx_Oracle'
# 'python2-cx_Oracle' is provided by 'cx_Oracle'
%if 0%{?rhel} == 8
Name: %{pkgname}
BuildRequires: python2
BuildRequires: python2-devel
Provides: python2-%{pkgname}
%else%
# On el7:
# the primary package built is 'cx_Oracle' for
# backwards compatibility reasons
# 'python2-cx_Oracle' is provided by 'cx_Oracle'
# the sub package built is 'python3-cx_Oracle'
Name: %{pkgname}
Provides: python-%{pkgname}
BuildRequires: python
BuildRequires: python-devel
%endif
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: python%{python3_pkgversion}
BuildRequires: oracle-%{oracleicname}-devel
BuildRequires: gcc

Summary: Python interface to Oracle
Version: 7.1
Release: 7%{?dist}
Source0: cx_Oracle-%{version}.tar.gz
License: Python Software Foundation License
Group: Development/Libraries
BuildRoot: %{_tmppath}/cx_Oracle-%{version}-%{release}-buildroot
Prefix: %{_prefix}
Url: http://cx-oracle.sourceforge.net
AutoReq: 0
Provides: python(:DBAPI:oracle) = 2.0
Requires: oracle-%{oracleicname}-basic >= %{oracleicver}

# Not needed after python-cx_Oracle -> cx_Oracle
# Obsoletes: cx_Oracle

%description
Python interface to Oracle conforming to the Python DB API 2.0 specification.
See http://www.python.org/topics/database/DatabaseAPI-2.0.html.

%package -n python%{python3_pkgversion}-%{pkgname}
Summary: Python3 interface to Oracle
Requires: oracle-%{oracleicname}-basic >= %{oracleicver}
%description -n python%{python3_pkgversion}-%{pkgname}
Python3 interface to Oracle conforming to the Python DB API 2.0 specification.
See http://www.python.org/topics/database/DatabaseAPI-2.0.html.

%prep 
%setup -n cx_Oracle-%{version}

#kinda ugly but we need ORACLE_HOME to be set
%if "%{_lib}" == "lib64"
%define oracle_home /usr/lib/oracle/%{oraclever}/client64
%else
%define oracle_home /usr/lib/oracle/%{oraclever}/client
%endif

%build
export ORACLE_HOME=%{oracle_home}
export CFLAGS="$RPM_OPT_FLAGS"
#env CFLAGS="$RPM_OPT_FLAGS" python setup.py build
%if 0%{?rhel} == 8
python2 setup.py build
%else%
python setup.py build
%endif%

%install
export ORACLE_HOME=%{oracle_home}
%if 0%{?rhel} == 8
python2 setup.py install --root=$RPM_BUILD_ROOT --record=P2_INSTALLED_FILES
python3 setup.py install --root=$RPM_BUILD_ROOT --record=P3_INSTALLED_FILES
%else%
python setup.py install --root=$RPM_BUILD_ROOT --record=P2_INSTALLED_FILES
python3 setup.py install --root=$RPM_BUILD_ROOT --record=P3_INSTALLED_FILES
%endif%

%clean
rm -rf $RPM_BUILD_ROOT

%files -f P2_INSTALLED_FILES
%doc LICENSE.txt README.txt  samples test

%files -n python%{python3_pkgversion}-%{pkgname} -f P3_INSTALLED_FILES
%doc LICENSE.txt README.txt  samples test

# HACK: The below is a fix to avoid transaction conflicts against version 7.1.3
# which was not built via koji and has a different %files layout with regards to
# egg-info.
# When building outside of koji the egginfo is defined as a directory:
# rpm -qpl /root/rpmbuild/RPMS/x86_64/cx_Oracle-7.1-4.el7.cern.x86_64.rpm |grep cx_Oracle-7.1.0-py2.7.egg-info
#/usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info
#/usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info/PKG-INFO
#/usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info/SOURCES.txt
#/usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info/dependency_links.txt
#/usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info/top_level.txt
# when building through koji the egginfo is defined as a file
# rpm -ql cx_Oracle |grep egg 
#/usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info
# The fix is if a directory of egg-info exists, then simply remove it so that
# the rpm transaction succeeds ...
%pretrans
if [ -d /usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info ]; then
  rm -rf /usr/lib64/python2.7/site-packages/cx_Oracle-7.1.0-py2.7.egg-info
fi

%changelog
* Thu Jun 30 2022 Ben Morrice <ben.morrice@cern.ch> 7.1-7
- tweak requires for CMS usecase (removing explicit python3 dep)

* Tue Mar 17 2020 Ben Morrice <ben.morrice@cern.ch> 7.1-6
- add Requires oracle in python3 subpackage

* Fri Mar 06 2020 Ben Morrice <ben.morrice@cern.ch> 7.1-5
- add packaging fix to avoid transaction conflicts on version 7.1.3

* Tue Mar 03 2020 Ben Morrice <ben.morrice@cern.ch> 7.1.4
- add c8 support, along with python3 subpackages

* Tue Oct 15 2019 Ben Morrice <ben.morrice@cern.ch> 7.1-3.el7.cern
- build for 19.3 rls

* Thu Feb 07 2019 Thomas Oulevey <thomas.oulevey@cern.ch> 7.1-2.el7.cern
- Remove obsolete Obsolete:

* Thu Feb 07 2019 Thomas Oulevey <thomas.oulevey@cern.ch> 7.1-1.el7.cern
- Rebuild against oracle-instantclient-12c

* Wed Feb 06 2019 John Mcwalters <john.mcwalters@oracle.com> 7.1-1
- Update to 7.1

* Thu Sep 13 2018 Livy Ge <livy.ge@oracle.com> 7.0-1.0.1
- update oracle-instantclient to 18c

* Thu Sep 13 2018 Jingdong Lu <jingdong.lu@oracle.com> 7.0-1
- Update to 7.0

* Mon Jul 09 2018 Jingdong Lu <jingdong.lu@oracle.com> 6.4.1-1
- Update to 6.4.1

* Mon Jul 02 2018 Jingdong Lu <jingdong.lu@oracle.com> 6.4-1
- Update to 6.4

* Tue May 08 2018 Jingdong Lu <jingdong.lu@oracle.com> 6.3.1-1
- Update to 6.3.1

* Thu May 03 2018 Jacob Wang <jacob.wang@oracle.com> 6.3-1
- rebased to latest stable version 6.3

* Mon Jan 29 2018 Keshav Sharma <keshav.sharma@oracle.com> 6.0.2-2
- renamed to python-cx_Oracle

* Tue Sep 19 2017 Keshav Sharma <keshav.sharma@oracle.com> 6.0.2-1
- rebased to latest stable version

* Wed Jun 07 2017 Michael Mraka <michael.mraka@redhat.com> 5.3-1
- rebased to latest stable version

* Thu Jan 29 2015 Tomas Lestach <tlestach@redhat.com> 5.1.2-5
- we need to use the exact oracle instantclient version

* Thu Jan 29 2015 Tomas Lestach <tlestach@redhat.com> 5.1.2-4
- do not require exact version of oracle instantclient
- fixed tito build warning
- replace legacy name of Tagger with new one

* Fri Mar 15 2013 Michael Mraka <michael.mraka@redhat.com> 5.1.2-3
- fixed builder definition

* Mon Oct 22 2012 Michael Mraka
- Use the ReleaseTagger.
- rebuild with correct vendor

* Mon Oct 08 2012 Jan Pazdziora 5.1.2-1
- Rebase to cx_Oracle 5.1.2.

* Mon Oct 08 2012 Jan Pazdziora 5.0.4-2
- Require latest greatest oracle-instantclient11.2-*.
- %%defattr is not needed since rpm 4.4

* Fri Jan 07 2011 Jan Pazdziora <jpazdziora@redhat.com> 5.0.4-1
- cx_Oracle 5.0.4 with Oracle InstantClient 11g

